# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 15:52:43 2018

@author: Administrator
"""
import numpy as np 
import pandas as pd 

#import data tu file chua data
dataset=pd.read_csv("C:/Users/Administrator/Desktop/all/train.csv")
testset=pd.read_csv('C:/Users/Administrator/Desktop/all/test.csv')
label=dataset.iloc[0:890,1]
data=dataset.iloc[0:890,[2,4,5]]
testdat=testset.iloc[0:418,[1,3,4]]
x=[data,testdat]

for change in x:
    change['Sex']=change['Sex'].map({'female':0,'male':1}).astype(int)
    
#filling NA values
data=(data.fillna(0)) 
testdat=testdat.fillna(0)

from sklearn.model_selection import train_test_split

train_data,test_data,train_labels,test_labels=train_test_split(data,label,test_size=20)

#KNN
#Train
from sklearn import neighbors
clf=neighbors.KNeighborsClassifier(n_neighbors = 200, p = 2)
clf.fit(train_data, train_labels)
pred_KNN=clf.predict(test_data)
from sklearn.metrics import accuracy_score
print("Accuracy of KNN: %.2f %%" %(100*accuracy_score(test_labels, pred_KNN)))

#Prediction for test.csv
result=clf.predict(testdat)

#SVM

from sklearn.svm import SVC   
svc = SVC(kernel = 'linear', C = 100)
svc.fit(train_data, train_labels)
pred_Kernel_linear = svc.predict(test_data)
print("Accuracy of Kernel_linear: %.2f %%" %(100*accuracy_score(test_labels, pred_Kernel_linear)))
result=svc.predict(testdat)

#So lieu qua lon may khong chay dc
#svc2 = SVC(kernel='poly', degree = 3, gamma=1)
#svc2.fit(train_data, train_labels)
#pred_Kernel_poly = svc2.predict(test_data)
#print("Accuracy of Kernel_linear: %.2f %%" %(100*accuracy_score(test_labels, pred_Kernel_poly)))

from sklearn.neural_network import MLPClassifier
mlp = MLPClassifier(hidden_layer_sizes=(10),solver='sgd',learning_rate_init=0.01,max_iter=100)
mlp.fit(train_data, train_labels)
pred_ANN=mlp.predict(test_data)
print("Accuracy of ANN: %.2f %%" %(100*accuracy_score(test_labels, pred_ANN)))
import pandas as pd
pd.DataFrame(mlp.loss_curve_).plot()